import user from './user'
import article from './article'
import table from './table'

export default [
  ...user,
  ...article,
  ...table
]

